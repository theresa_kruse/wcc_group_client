import React, { ChangeEvent, useEffect, useState } from "react"
import { Button, TextField, List, ListItem, ListItemText, Container } from '@material-ui/core';
import { Add } from "@material-ui/icons";
import { Pie } from "react-chartjs-2";
import './drivers.css';

interface Car {car_id: string, driver_name: string}
interface Trip {destination: string, license: string, duration: number}

function CarList() {

    const [car_id, setCar_ID] = useState("")
    const [driver_name, setDriver_Name] = useState("")
    const [cars, setCars] = useState([] as Car[])
    const [trips, setTrips] = useState([] as Trip[])


    function loadCars() {
        fetch("/drivers")
        .then(data => data.json())
        .then(cars => setCars(cars) )
    }
    useEffect( loadCars, [] )

    function loadTrips() {
        fetch("/trips")
        .then(data => data.json())
        .then(trips => setTrips(trips) )
    }
    useEffect( loadTrips, [] )

    function createCar(car:Car) {
        fetch("/drivers", {

            method: "POST", headers: {
                'content-type': 'application/json;charset=UTF-8',
            },
            body: JSON.stringify(car) })
        .then( data => console.log(data) )
    }

    function changeCar_ID(x:ChangeEvent<HTMLInputElement>) {
        setCar_ID(x.target.value)
    }
    function changeDriver_Name(x:ChangeEvent<HTMLInputElement>) {
        setDriver_Name(x.target.value)
    }

    function submit(e:any) {
        alert("Reload page to see updated drivers.")
        e.preventDefault()
        let car = {car_id: car_id, driver_name: driver_name}

        createCar(car)
        setCars([...cars, car])
    }

    function update(e:any) {
        alert("Reload page to see updated drivers. ")
        let car = {car_id: car_id, driver_name: driver_name}
        updateCar(car)
        setCars([...cars, car])
    }

    function updateCar(car:Car) {
        //fetch(`/drivers/${car.car_id}, {
        fetch(`/drivers`, {
            method: "PUT", headers: {
                'content-type': 'application/json;charset=UTF-8',
            },
            body: JSON.stringify(car) })
        .then( data => console.log(data) )
    }

    function CarChart() {
        const backgroundColours = [
            'rgba(54, 162, 235, 0.2)',
            'rgba(255, 99, 132, 0.2)',
            'rgba(255, 206, 86, 0.2)',
            'rgba(75, 192, 192, 0.2)',
            'rgba(153, 102, 255, 0.2)',
            'rgba(255, 159, 64, 0.2)'
        ]
        
        const colours = [
            'rgba(54, 162, 235, 1)',
            'rgba(255, 99, 132, 1)',
            'rgba(255, 206, 86, 1)',
            'rgba(75, 192, 192, 1)',
            'rgba(153, 102, 255, 1)',
            'rgba(255, 159, 64, 1)'
        ]

        let licenses:String[] = []
        for  (const i in trips) {
            const trip = trips[i]
            if (!licenses.includes(trip.license)) {
                licenses.push(trip.license)
            }
        }

        let values = []
        for (const i in licenses) {
            let count = 0
            for( const j in trips)
                if ( trips[j].license == licenses[i])
                    count++
            values.push(count)
        }

        let labels_info: String[] = []
        for (const i in licenses) {
            const license_string = licenses[i]
            for (const j in cars) {
                const driver = cars[j]
                if (license_string == driver.car_id) {
                    if(!labels_info.includes(license_string)) {
                        const new_string = driver.car_id
                        labels_info.push(new_string)
                    }
                }

            }
        }

        const config = {
            labels: labels_info,
            datasets: [{
                label: 'Number of trips per car',
                data: values,
                backgroundColor: backgroundColours,
                borderColor: colours,
                borderWidth: 1
            }]
        }

        return <Pie
        data={config}
        width={200}
        height={200}
    />
    }



    return <Container id="drive">
        <List id="list">
            {
                cars.map((t, i) => {
                    return <ListItem key={i}>
                        <ul>
                            <ListItemText id="a">
                            <li>
                            License: {t.car_id} - Driver: {t.driver_name}
                            </li>
                            </ListItemText>
                            </ul>


                    </ListItem>
                            
                })
            }
        </List>
        <form onSubmit={submit} id="drive_form">
            <TextField label="License" 
                       onChange={changeCar_ID} 
                       value={car_id} 
                       InputLabelProps={{ shrink: true, }}/>
            <TextField label="Driver" 
                       onChange={changeDriver_Name} 
                       value={driver_name} 
                       InputLabelProps={{ shrink: true, }}/>
            <Button color="secondary" 
                    variant={"contained"} 
                    startIcon={<Add />} 
                    onClick={submit}>
                Add Driver
            </Button>
            <Button color="secondary" 
                    variant={"contained"} 
                    startIcon={<Add />} 
                    onClick={update}>
                Update
            </Button>
        </form>
        <CarChart></CarChart>
    </Container>
}

export default CarList