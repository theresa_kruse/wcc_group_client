import { ChangeEvent, useEffect, useState } from "react"
import { Button, TextField, List, ListItem, ListItemText, Container } from '@material-ui/core';
import { Add } from "@material-ui/icons";
import { Bar } from "react-chartjs-2";
import './trips.css';


interface Trip { destination: string, license: string, duration: number}
interface Car {car_id: string, driver_name: string}

function TripList() {

    const [destination, setDestination] = useState("")
    const [license, setLicense] = useState("")
    const [duration, setDuration] = useState(30)
    const [trips, setTrips] = useState([] as Trip[])

    const [car_id, setCar_Id] = useState("")
    const [cars, setCars] = useState([] as Car[])


    function loadTrips() {
        fetch("/trips")
        .then(data => data.json())
        .then(trips => setTrips(trips) )
    }
    useEffect( loadTrips, [] )

    function loadCars() {
        fetch("/drivers")
        .then(data => data.json())
        .then(trips => setCars(cars) )
    }
    useEffect( loadCars, [] )

    function createTrip(trip:Trip) {
        fetch("/trips", {
            method: "POST", headers: {
                'content-type': 'application/json;charset=UTF-8',
            },
            body: JSON.stringify(trip) })
        .then( data => console.log(data) )
    }

    function createCar(car:Car) {
        fetch("/drivers", {
            method: "POST", headers: {
                'content-type': 'application/json;charset=UTF-8',
            },
            body: JSON.stringify(car) })
        .then( data => console.log(data) )
    }

    function changeDestination(x:ChangeEvent<HTMLInputElement>) {
        setDestination(x.target.value)
    }
    function changeLicense(x:ChangeEvent<HTMLInputElement>) {
        setLicense(x.target.value)
        setCar_Id(x.target.value)
    }
    function changeDuration(x:ChangeEvent<HTMLInputElement>) {
        setDuration(parseInt(x.target.value))
    }

    function submit(e:any) {
        alert("Reload page to see updates in drivers graph.")
        e.preventDefault()
        let trip = { destination: destination, license: license, duration: duration}
        let car = {car_id: license, driver_name: ""}
        createTrip(trip)
        createCar(car)
        setTrips( [...trips, trip] )
        setCars( [...cars, car] )
    }

    function TripChart() {
        const backgroundColours = [
            'rgba(54, 162, 235, 0.2)',
            'rgba(255, 99, 132, 0.2)',
            'rgba(255, 206, 86, 0.2)',
            'rgba(75, 192, 192, 0.2)',
            'rgba(153, 102, 255, 0.2)',
            'rgba(255, 159, 64, 0.2)'
        ]
        
        const colours = [
            'rgba(54, 162, 235, 1)',
            'rgba(255, 99, 132, 1)',
            'rgba(255, 206, 86, 1)',
            'rgba(75, 192, 192, 1)',
            'rgba(153, 102, 255, 1)',
            'rgba(255, 159, 64, 1)'
        ]

        let cities:String[] = []
        for  (const i in trips) {
            const trip = trips[i]
            console.log(trip.destination)
            if (!cities.includes(trip.destination)) {
                cities.push(trip.destination)
            }
        }

        let values = []
        for (const i in cities) {
            let count = 0
            for( const j in trips)
                if ( trips[j].destination == cities[i])
                    count++
            values.push(count)
        }

        const config = {
            labels: cities,
            datasets: [{
                label: 'Number of trips per destination',
                data: values,
                backgroundColor: backgroundColours,
                borderColor: colours,
                borderWidth: 1
            }]
        }

        return <Bar
        data={config}
        width={200}
        height={200}
        options={{ scales: { yAxes: [{ ticks: { beginAtZero: true, stepSize: 1 } }] }}}
    />
    }

    return <Container>
        <List id="list">
            {
                trips.map((t, i) => {

                    return <ListItem key={i}>
                        <ul>
                                    <ListItemText id="a">
                                        <li>
                                    Destination: {t.destination} - License: {t.license} - Duration: {t.duration} minutes
                                    </li>
                                    </ListItemText>
                                    </ul>
                                </ListItem>
                })
            }
        </List>
        <form onSubmit={submit} id="trip_form">
            <TextField label="Destination" 
                       onChange={changeDestination} 
                       value={destination} 
                       InputLabelProps={{ shrink: true, }}/>
            <TextField label="License Plate" 
                       onChange={changeLicense} 
                       value={license} 
                       InputLabelProps={{ shrink: true, }}/>
            <TextField label="Duration (in minutes)" 
                       onChange={changeDuration} 
                       value={duration} 
                       InputLabelProps={{ shrink: true, }}/>
            <Button color="primary" 
                    variant={"contained"} 
                    startIcon={<Add />} 
                    onClick={submit}>
                Add trip
            </Button>
        </form>
        <TripChart
    />
    </Container>
    
}

export default TripList