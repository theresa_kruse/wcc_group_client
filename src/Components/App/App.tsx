import './App.css';
import { Container } from '@material-ui/core';
import TripOverview from '../Trips/trips';
import DriverOverview from '../Drivers/drivers'
import logo from './logo.png';
import reload from './reload.png'
import { Button } from '@material-ui/core';

function App() {
  return (
    <>
    <header id="header">
    <img src={logo} alt="logo" id="logo"></img>
      <section>
        <h2>Fast Trips Services</h2>
        <h4>Jacob Lind, Arne Wawers, Theresa Kruse</h4>
        </section>
        <Button
                    variant={"contained"} 
                    style={{ borderRadius: 50, margin: 40, height: 70, width: 70, }}
                    onClick={() => window.location.reload()} ><img src={reload} alt="reload" id="reload"></img> </Button>
    </header>
  

    <Container id="forms">
      <section id="trips"><TripOverview/> </section>
      <section id="drivers"><DriverOverview/> </section>
    </Container>

    <footer>
        <p>&copy; 2021 FastTripsService. All rights reserved.</p>
        <p>Reach us under this email:<button id="email_button"><a href="mailto:team@fasttrips.pt">team@fasttrips.pt</a></button> </p>
    </footer>
    </>
  );
}

export default App;
